var gulp = require('gulp'),
	browserify = require('gulp-browserify'),
	concat = require('gulp-concat');

gulp.task('scripts', function() {
	// Single entry point to browserify
	gulp.src('./src/js/*.js')
		.pipe(concat('main.js'))
		.pipe(browserify({}))
		.pipe(gulp.dest('./static/js'))
});

gulp.task('scripts2', function() {
	// Single entry point to browserify
	gulp.src([
			'./src/js/*.js',
			'./node_modules/@persgroep/**/*.js'
		])
		.pipe(concat('main.js'))
		.pipe(browserify({}))
		.pipe(gulp.dest('./static/js'))
});
